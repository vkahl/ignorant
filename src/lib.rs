// This implementation is an altered version of what is being shown in this
// video by Jon Gjengset: https://www.youtube.com/watch?v=b4mS5UPHh20

use std::sync::Arc;

use parking_lot::{Condvar, Mutex};

struct Shared<T> {
    inner: Mutex<Inner<T>>,
    available: Condvar,
}

struct Inner<T> {
    slot: Option<T>,
    closed: bool,
}

impl<T> Default for Inner<T> {
    fn default() -> Self {
        Inner {
            slot: None,
            closed: false,
        }
    }
}

pub struct IgnorantSender<T> {
    shared: Arc<Shared<T>>,
}

impl<T> IgnorantSender<T> {
    pub fn send(&mut self, item: T) -> Option<T> {
        let mut old_item = Some(item);
        {
            let locked_slot = &mut self.shared.inner.lock().slot;
            std::mem::swap(&mut old_item, locked_slot);
        }
        self.shared.available.notify_one();
        old_item
    }

    pub fn checked_send(&mut self, item: T) -> Result<Option<T>, ReplaceError> {
        if Arc::strong_count(&self.shared) > 1 {
            Ok(self.send(item))
        } else {
            Err(ReplaceError::Disconnected)
        }
    }

    pub fn is_empty(&self) -> bool {
        self.shared.inner.lock().slot.is_none()
    }

    pub fn holds_value(&self) -> bool {
        !self.is_empty()
    }
}

impl<T> Drop for IgnorantSender<T> {
    fn drop(&mut self) {
        self.shared.inner.lock().closed = true;
        self.shared.available.notify_all();
    }
}

pub struct IgnorantReceiver<T> {
    shared: Arc<Shared<T>>,
}

// Implemented explicitly because deriving it would only allow `T: Clone`
impl<T> Clone for IgnorantReceiver<T> {
    fn clone(&self) -> Self {
        IgnorantReceiver {
            shared: Arc::clone(&self.shared),
        }
    }
}

impl<T> IgnorantReceiver<T> {
    pub fn recv(&self) -> Result<T, ReceiveError> {
        let inner = &mut self.shared.inner.lock();
        loop {
            if let Some(item) = inner.slot.take() {
                return Ok(item);
            } else if inner.closed {
                return Err(ReceiveError::Disconnected);
            } else {
                self.shared.available.wait(inner);
            }
        }
    }

    pub fn try_recv(&self) -> Result<T, ReceiveError> {
        if let Some(mut inner) = self.shared.inner.try_lock() {
            if inner.closed {
                Err(ReceiveError::Disconnected)
            } else {
                inner.slot.take().ok_or(ReceiveError::Empty)
            }
        } else {
            Err(ReceiveError::Empty)
        }
    }

    pub fn recv_timeout(&self, timeout: std::time::Duration) -> Result<T, ReceiveError> {
        let tic = std::time::Instant::now();
        let inner = &mut self.shared.inner.lock();
        while tic.elapsed() < timeout {
            if let Some(item) = inner.slot.take() {
                return Ok(item);
            } else if inner.closed {
                return Err(ReceiveError::Disconnected);
            } else if !self.shared.available.wait_for(inner, timeout).timed_out() {
                if let Some(item) = inner.slot.take() {
                    return Ok(item);
                } else if inner.closed {
                    return Err(ReceiveError::Disconnected);
                }
            }
        }
        Err(ReceiveError::Timeout)
    }
}

pub fn ignorant_channel<T>() -> (IgnorantSender<T>, IgnorantReceiver<T>) {
    let shared = Shared {
        inner: Mutex::default(),
        available: Condvar::new(),
    };
    let sender = IgnorantSender {
        shared: Arc::new(shared),
    };
    let ignorant_receiver = IgnorantReceiver {
        shared: sender.shared.clone(),
    };
    (sender, ignorant_receiver)
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum ReceiveError {
    Empty,
    Disconnected,
    Timeout,
}

impl std::fmt::Display for ReceiveError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            ReceiveError::Empty => f.write_str("The ignorant channel is empty"),
            ReceiveError::Disconnected => f.write_str("The sender has been dropped"),
            ReceiveError::Timeout => f.write_str("Nothing has been received within timeout"),
        }
    }
}

impl std::error::Error for ReceiveError {}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum ReplaceError {
    Disconnected,
}

impl std::fmt::Display for ReplaceError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            ReplaceError::Disconnected => f.write_str("The receiver has been dropped"),
        }
    }
}

impl std::error::Error for ReplaceError {}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn basic_test() {
        let (mut sender, receiver) = ignorant_channel();
        let receiver2 = receiver.clone();
        assert!(sender.is_empty());
        assert_eq!(sender.send(1), None);
        assert!(sender.holds_value());
        assert_eq!(sender.send(2), Some(1));
        assert_eq!(sender.checked_send(3), Ok(Some(2)));
        assert_eq!(receiver.recv(), Ok(3));
        sender.send(4);
        sender.send(5);
        assert_eq!(receiver2.try_recv(), Ok(5));
        assert_eq!(receiver.try_recv(), Err(ReceiveError::Empty));
    }

    #[test]
    fn dropped_sender() {
        let (mut sender, receiver) = ignorant_channel();
        let receiver2 = receiver.clone();
        sender.send(1);
        drop(sender);
        assert_eq!(receiver2.recv(), Ok(1));
        assert_eq!(receiver.recv(), Err(ReceiveError::Disconnected));
    }

    #[test]
    fn dropped_receiver() {
        let (mut sender, receiver) = ignorant_channel();
        let receiver2 = receiver.clone();
        sender.send(1);
        drop(receiver);
        assert_eq!(sender.send(2), Some(1));
        assert_eq!(sender.checked_send(3), Ok(Some(2)));
        drop(receiver2);
        assert_eq!(sender.checked_send(4), Err(ReplaceError::Disconnected));
    }

    #[test]
    fn timeout() {
        let (mut sender, receiver) = ignorant_channel();
        sender.send(1);
        assert_eq!(sender.send(2), Some(1));
        assert_eq!(receiver.recv(), Ok(2));
        std::thread::spawn(move || {
            std::thread::sleep(std::time::Duration::from_millis(500));
            sender.send(3);
        });
        assert_eq!(
            receiver.recv_timeout(std::time::Duration::from_millis(10)),
            Err(ReceiveError::Timeout)
        );
        assert_eq!(
            receiver.recv_timeout(std::time::Duration::from_secs(1)),
            Ok(3)
        );
    }
}
